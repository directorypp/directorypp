#ifndef TYPE_H
#define TYPE_H

#include <ostream>
#include <ctime>
#include <map>

#include <boost/shared_ptr.hpp>

namespace type
{

class content
{
	friend std::ostream& operator<< (std::ostream& os, boost::shared_ptr<content> c);
	virtual void print(std::ostream& os) const = 0;

public:
	virtual ~content() = 0;
};

template <typename T, typename B>
struct cloneable: public B
{
	virtual B* clone() const
	{
		return new T;
	}
};


class file : public content
{
public:
	file () :
		m_file()
	{}

	void set_file(std::string const& file)
	{
		m_file = file;
	}
	virtual file* clone() const = 0;

private:
	void print(std::ostream & os) const;

protected:
	std::string m_file;

};

class html: public cloneable<html, file>
{
public:
	html ()
	{}

private:
	void print(std::ostream& os) const;
};

class text : public cloneable<text, file>
{
public:
	text ()
	{}

protected:
	std::string escape (std::string const& s) const;

private:
	void print(std::ostream& os) const;
};

class code : public cloneable<code, text>
{
	void print(std::ostream& os) const;
};


/**A very primitive cpp syntax highlighter.
  It only makes keywords colourful and strong.*/
class cpp : public cloneable<cpp, code>
{
	typedef std::map<std::string, std::string> map; // maps a keyword to a color
	map keywords;

	void print(std::ostream& os) const;

public:
	cpp();
};

} // namespace type

#endif
