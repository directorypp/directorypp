#ifndef ITEM_H
#define ITEM_H

namespace type {

struct positionitem
{
	std::string title;
	std::string link;
};

struct banneritem
{
	std::string link;
	std::string image_file;
	std::string image_extension;
	std::string alt_text;
};

struct menuitem
{
	std::string type; ///< css class which should be used
	std::string name;
	std::string link;
};

struct item
{
	std::string title;

	std::string author;
	std::string generator;
	std::string language;
	std::string copyright;

	time_t time;
	std::tm localtime;
	std::string pubDate; // TODO newsitem?
};

struct newsitem : item
{
	std::string link;
	std::string description;
	std::string guid;

	std::string filename;

	boost::shared_ptr<type::content> m;
};

struct imageitem : item
{
	std::string comment;
	std::string alt_text;
	int id;

	std::string filename_thumbnail_file;
	std::string filename_thumbnail;
	std::string filename_extension;
	std::string filename_full_file;
	std::string filename_full;

	std::string resolution;
};

} // namespace type

#endif
