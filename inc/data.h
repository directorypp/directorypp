#ifndef CONTENT_H
#define CONTENT_H

#include <cppcms/view.h>

#include <boost/shared_ptr.hpp>

#include <ctime>
#include <vector>

#include "type.h"
#include "item.h"

namespace content
{

struct message : public cppcms::base_content
{
	message ();

	std::string base;
	std::string title;
	std::string meta;
	std::string date;

	std::string filename;

	std::vector<type::positionitem>position_items;
	std::vector<type::menuitem>menu_items;
	std::vector<type::banneritem>banner_items;
};

struct contentmessage : public message
{
	contentmessage (boost::shared_ptr<type::content> c);

	boost::shared_ptr<type::content> m;
};

struct newsmessage : public message
{
	std::vector<type::newsitem> news_items;
};

struct imagemessage : public message
{
	std::vector<type::imageitem> image_items;
};

} // namespace data

#endif
