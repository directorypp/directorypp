#include "hello.h"

int main(int argc, char **argv) try
{
	cppcms::service srv(argc,argv);
	srv.applications_pool().mount(cppcms::applications_factory<hello>());
	srv.run();
	/*
	cppcms::manager app(argc, argv);
	app.set_worker(new cppcms::application_factory<hello>());
	app.execute();
	*/

	return 0;
}
catch (std::exception const& e)
{
	std::cerr << e.what() << std::endl;
	return 1;
}
