#include "debug.h"

#include <iostream>

#define QUOTE(name) #name
#define PRINT(x) std::cout << QUOTE(x) << ": " << x << ::std::endl;

void debug_request(cppcms::http::request const& crequest)
{
	cppcms::http::request &request =
		const_cast<cppcms::http::request&>(crequest);

	std::cout << " = Information =" << std::endl;
	PRINT(request.auth_type());
	PRINT(request.content_length());
	PRINT(request.content_type());
	PRINT(request.gateway_interface());
	PRINT(request.path_info());
	PRINT(request.path_translated());
	PRINT(request.query_string());
	PRINT(request.request_method());
	PRINT(request.script_name());
	/*
	PRINT(request.remote_addr());
	PRINT(request.remote_host());
	PRINT(request.remote_ident());
	PRINT(request.remote_user());
	PRINT(request.server_port());
	PRINT(request.server_protocol());
	PRINT(request.server_software());
	*/

	/*
	typedef std::map<std::string,std::string> env_t;
	env_t env = request.getenv();
	for (env_t::iterator it=env.begin(); it!=env.end(); ++it)
	{
		std::cout << it->first << "=" << it->second << std::endl;
	}
	*/


	/*
	std::cout << " = Server Information" << std::endl;
	std::cout << "request.getServerSoftware()" << request.getServerSoftware() << std::endl;
	std::cout << "request.getServerName()" << request.getServerName() << std::endl;
	std::cout << "request.getGatewayInterface()" << request.getGatewayInterface() << std::endl;
	std::cout << "request.getServerProtocol()" << request.getServerProtocol() << std::endl;
	std::cout << "request.getServerPort()" << request.getServerPort() << std::endl;
	std::cout << "request.usingHTTPS()" << request.usingHTTPS() << std::endl;

	std::cout << " = CGI Query Information" << std::endl;
	std::cout << "request.getCookies()" << request.getCookies() << std::endl;
	std::cout << "request.getRequestMethod()" << request.getRequestMethod() << std::endl;
	std::cout << "request.getPathInfo()" << request.getPathInfo() << std::endl;
	std::cout << "request.getPathTranslated()" << request.getPathTranslated() << std::endl;
	std::cout << "request.getScriptName()" << request.getScriptName() << std::endl;
	std::cout << "request.getQueryString()" << request.getQueryString() << std::endl;
	std::cout << "request.getContentLength()" << request.getContentLength() << std::endl;
	std::cout << "request.getContentType()" << request.getContentType() << std::endl;
	std::cout << "request.getPostData()" << request.getPostData() << std::endl;

	std::cout << " = Server Specific Information" << std::endl;
	std::cout << "request.getReferrer()" << request.getReferrer() << std::endl;

	std::cout << " = Remote User Information" << std::endl;
	std::cout << "request.getRemoteHost()" << request.getRemoteHost() << std::endl;
	std::cout << "request.getRemoteAddr()" << request.getRemoteAddr() << std::endl;
	std::cout << "request.getAuthType()" << request.getAuthType() << std::endl;
	std::cout << "request.getRemoteUser()" << request.getRemoteUser() << std::endl;
	std::cout << "request.getRemoteIdent()" << request.getRemoteIdent() << std::endl;
	std::cout << "request.getAccept()" << request.getAccept() << std::endl;
	std::cout << "request.getUserAgent()" << request.getUserAgent() << std::endl;

	std::cout << " = ErrorDocument Handling" << std::endl;
	std::cout << "request.getRedirectRequest()" << request.getRedirectRequest() << std::endl;
	std::cout << "request.getRedirectURL()" << request.getRedirectURL() << std::endl;
	std::cout << "request.getRedirectStatus()" << request.getRedirectStatus() << std::endl;

	std::cout << " = End of Debug Information" << std::endl;
	*/
}
