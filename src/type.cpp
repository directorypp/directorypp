#include "type.h"

#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

#include <iostream>
#include <sstream>
#include <fstream>

namespace fs = boost::filesystem;

namespace type
{

content::~content()
{}

std::ostream& operator<< (std::ostream& os, boost::shared_ptr<content> c)
{
	if (c.get() != 0) c->print(os);
	else os << "no content";
	return os;
}

void content::print(std::ostream &os) const
{
	os << "standard content\n";
}

void file::print(std::ostream &os) const
{
	os << "file\n";
}

void html::print(std::ostream &os) const
{
	std::ifstream fin (m_file.c_str());

	std::string line;
	while (getline(fin, line)) os << line << "\n";
}

std::string text::escape(std::string const& s) const
{
	// TODO: copied from cppcms source
	std::string content;
	unsigned i,len=s.size();
	content.reserve(len*3/2);
	for(i=0;i<len;i++) {
		char c=s[i];
		switch(c){
			case '<': content+="&lt;"; break;
			case '>': content+="&gt;"; break;
			case '&': content+="&amp;"; break;
			case '\"': content+="&quot;"; break;
			default: content+=c;
		}
	}
	return content;
}

void text::print(std::ostream &os) const
{
	std::ifstream fin (m_file.c_str());

	os << "<pre>\n";

	std::string line;
	while (getline(fin, line)) os << escape(line) << "\n";

	os << "</pre>\n";
}

void code::print(std::ostream &os) const
{
	std::ifstream fin (m_file.c_str());

	os << "<pre>\n";
	os << "<code>\n";

	std::string line;
	while (getline(fin, line))
	{
		os << escape(line) << "\n";
	}

	os << "</code>\n";
	os << "</pre>\n";
}

cpp::cpp()
{
	keywords["and"] = "brown";
	keywords["and_eq"] = "brown";
	keywords["asm"] = "brown";
	keywords["auto"] = "green";
	keywords["bitand"] = "brown";
	keywords["bitor"] = "brown";
	keywords["bool"] = "green";
	keywords["break"] = "brown";
	keywords["case"] = "brown";
	keywords["catch"] = "brown";
	keywords["char"] = "green";
	keywords["class"] = "green";
	keywords["compl"] = "brown";
	keywords["const"] = "green";
	keywords["const_cast"] = "brown";
	keywords["continue"] = "brown";
	keywords["default"] = "brown";
	keywords["delete"] = "brown";
	keywords["do"] = "brown";
	keywords["double"] = "green";
	keywords["dynamic_cast"] = "brown";
	keywords["else"] = "brown";
	keywords["enum"] = "green";
	keywords["explicit"] = "green";
	keywords["export"] = "green";
	keywords["extern"] = "green";
	keywords["false"] = "red";
	keywords["float"] = "green";
	keywords["for"] = "brown";
	keywords["friend"] = "brown";
	keywords["goto"] = "brown";
	keywords["if"] = "brown";
	keywords["inline"] = "green";
	keywords["int"] = "green";
	keywords["long"] = "green";
	keywords["mutable"] = "green";
	keywords["namespace"] = "green";
	keywords["new"] = "brown";
	keywords["not"] = "brown";
	keywords["not_eq"] = "brown";
	keywords["operator"] = "brown";
	keywords["or"] = "brown";
	keywords["or_eq"] = "brown";
	keywords["private"] = "brown";
	keywords["protected"] = "brown";
	keywords["public"] = "brown";
	keywords["register"] = "green";
	keywords["reinterpret_cast"] = "brown";
	keywords["return"] = "brown";
	keywords["short"] = "green";
	keywords["signed"] = "green";
	keywords["sizeof"] = "brown";
	keywords["static"] = "green";
	keywords["static_cast"] = "brown";
	keywords["struct"] = "green";
	keywords["switch"] = "brown";
	keywords["template"] = "green";
	keywords["this"] = "brown";
	keywords["throw"] = "brown";
	keywords["true"] = "red";
	keywords["try"] = "brown";
	keywords["typedef"] = "green";
	keywords["typeid"] = "brown";
	keywords["typename"] = "green";
	keywords["union"] = "green";
	keywords["unsigned"] = "green";
	keywords["using"] = "green";
	keywords["virtual"] = "green";
	keywords["void"] = "green";
	keywords["volatile"] = "green";
	keywords["wchar_t"] = "green";
	keywords["while"] = "brown";
	keywords["xor"] = "brown";
	keywords["xor_eq"] = "brown";
}


void cpp::print(std::ostream &os) const
{
	std::ifstream fin (m_file.c_str());

	os << "<pre>\n";
	os << "<code>\n";

	std::string line;
	while (getline(fin, line))
	{
		std::string escaped_line = escape(line);

		std::istringstream is (line);
		std::string word;
		while (is >> word)
		{

			map::const_iterator it = keywords.find(word);

			if (it != keywords.end())
			{
				escaped_line = boost::regex_replace(escaped_line,
						boost::regex("(.*)"+it->first+"(.*)"),
						"\\1<strong><font color=\""+it->second+"\">"+it->first+"</font></strong>\\2",
						boost::match_all | boost::format_sed);
			}
		}
		os << escaped_line << "\n";
	}

	os << "</code>\n";
	os << "</pre>\n";
}

} // namespace type
