#ifndef FS_H
#define FS_H

#include <fstream>

#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

#endif
