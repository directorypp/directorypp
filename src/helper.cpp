#include "helper.h"

/**Reads a file and return content as string.
  Uses find_file to search for a file recursively.
  Lines will be concatenated with '\n' linebreaks.
  */
std::string read_file (fs::path where, fs::path file)
{
	fs::path real_file = find_file(where, file);
	// read in the whole content of the file and return it
	fs::ifstream ftitle (real_file);
	if (!ftitle.is_open()) return "";
	std::string content;
	std::string line;
	std::getline(ftitle, line);
	content = line; // we dont want a endline in the end,
	// this is already provided in the template
	while (std::getline(ftitle, line)) content += "\n" + line;
	return content;

}

/**Finds the file file anywhere in and above where.
  @param where to look for the file
  @param file the filename
  @pre file is not allowed to have path separators
  Will search recursively for a file.
  Also the root directory will be searched.
  @example
  where = "my/deep/path";
  file = "date.txt"
  following files will be searched (in that order):
  my/deep/path/date.txt
  my/deep/date.txt
  my/date.txt
  ./date.txt
  @endexample
  */
fs::path find_file (fs::path where, fs::path file)
{
	bool finished = false;

	do {
		if (fs::exists(where / file))
		{
			return where / file;
		}
		where = where.branch_path(); // go one directory up
		if (where.empty()) where = "."; // also search root directory
		else if (where == ".") finished = true; // after root directory we are finished
	} while (!finished);
	return fs::path();
}
