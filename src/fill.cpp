#include "hello.h"

#include "fs.h"
#include "helper.h"

#include <boost/bind.hpp>

void hello::fill_position(std::string const& file, std::vector<type::positionitem>& position_items) const
{
	fs::path where(file);

	where = where.branch_path(); // dont put a position for the file itself

	while (!where.empty())
	{
		type::positionitem pi;

		pi.title = where.leaf();
		pi.link = "/" + where.string() + "/";

		position_items.push_back(pi);

		where = where.branch_path(); // go one directory up
	}

	std::reverse(position_items.begin(), position_items.end());
}

void hello::fill_message(std::string const& file, content::message & msg) const
{
	fs::path my_path(file);
	if (fs::is_directory(my_path))
	{
		// handle special folders
		if (my_path.leaf() == "..") msg.base = base_url + my_path.branch_path().string();
		else msg.base = base_url + (my_path / "index").string();
	}
	else msg.base = base_url + file;

	msg.title = read_file(file, "title.txt");
	msg.meta = read_file(file, "meta.html");
	msg.date = read_file(file, "date.txt");
	msg.filename = file;
	fill_position(file, msg.position_items);
	fill_banner(std::string("banner"), msg.banner_items);
}

void hello::fill_images(std::string const& file,
		std::vector<type::imageitem>& image_items) const
{
	fs::path my_path(file);
	fs::directory_iterator dend;

	int id=0;

	for (fs::directory_iterator dit(my_path); dit != dend; ++dit)
	{
		map::const_iterator it;
		std::string file_extension = extension(*dit);
		it = transformer.find(file_extension);
		if (it == transformer.end())
		{
			throw no_transformation_for_file_extension(file_extension);
		}
		file_information info = it->second;

		if (!info.binary_file) continue;

		type::imageitem image_item;
		image_item.title = basename(*dit);

		cppcms::json::value config = const_cast<hello*>(this)->settings();
		image_item.author =    config.get<std::string>("server.author");
		image_item.generator = config.get<std::string>("server.generator");
		image_item.language =  config.get<std::string>("server.language");
		image_item.copyright = config.get<std::string>("server.copyright");

		image_item.time = fs::last_write_time(*dit);
		// TODO: Time
		// image_item.localtime = *localtime(&image_item.time);
		// image_item.pubDate = "";

		// image specific attributes
		image_item.comment = "No comment";
		image_item.alt_text = image_item.title;

		image_item.id = id++;
		image_item.filename_thumbnail_file = "/" + dit->string() + "?file";
		image_item.filename_thumbnail = "/" + dit->string();
		image_item.filename_extension = file_extension;
		image_item.filename_full_file = "/pic/" + dit->string() + "?file";
		image_item.filename_full = "/pic/" + dit->string();

		image_item.resolution = "200x200";

		image_items.push_back(image_item);
	}

	// dont sort by time, otherwise id is confused
}

void hello::fill_banner(std::string const& file, std::vector<type::banneritem>& banner_items) const
{
	fs::path my_path(file);
	fs::directory_iterator dend;

	for (fs::directory_iterator dit(my_path); dit != dend; ++dit)
	{
		map::const_iterator it;
		std::string file_extension = extension(*dit);
		it = transformer.find(file_extension);
		if (it == transformer.end())
		{
			throw no_transformation_for_file_extension(file_extension);
		}
		file_information info = it->second;

		if (!info.binary_file) continue;

		std::string info_file = dit->path().string() + ".txt";
		fs::ifstream fin(info_file);
		std::string link;
		std::getline(fin, link);

		type::banneritem banner_item;
		banner_item.link = link;
		banner_item.image_file = "/" + dit->string() + "?file";
		banner_item.image_extension = file_extension;
		banner_item.alt_text = basename(*dit);

		banner_items.push_back(banner_item);
	}
}

void hello::fill_news(std::string const& file, std::vector<type::newsitem>& news_items) const
{
	fs::path my_path(file);
	fs::directory_iterator dend;

	for (fs::directory_iterator dit(my_path); dit != dend; ++dit)
	{
		std::string file_extension = fs::extension(*dit);

		map::const_iterator it;
		it = transformer.find(file_extension);
		if (it == transformer.end())
		{
			throw no_transformation_for_file_extension(file_extension);
		}
		file_information info = it->second;

		boost::shared_ptr<type::file> f(info.ptr->clone());
		f->set_file(dit->string()); // set string to file

		type::newsitem news_item;
		news_item.title = basename(*dit);
		news_item.link = base_url + dit->string();
		news_item.guid = news_item.link;
		cppcms::json::value config = const_cast<hello*>(this)->settings();
		news_item.author = config.get<std::string>("server.author");
		news_item.generator = config.get<std::string>("server.generator");
		news_item.language = config.get<std::string>("server.language");
		news_item.copyright = config.get<std::string>("server.copyright");
		news_item.filename = dit->string();
		news_item.time = fs::last_write_time(*dit);
		// TODO: Time
		// news_item.localtime = *localtime(&news_item.time);
		// news_item.pubDate = "";
		news_item.m = f;

		news_items.push_back(news_item);
	}

	std::sort(news_items.begin(), news_items.end(),
			boost::bind(&type::newsitem::time, _1) > boost::bind(&type::newsitem::time, _2) ); // sort by time
}

void hello::fill_menu(std::string const& m_file, std::vector<type::menuitem>& menu_items) const
{
	fs::directory_iterator end_iter;
	fs::path my_path(m_file);
	fs::path my_file(m_file);
	bool is_file = false;
	bool is_special_folder = false;

	// if we don't print a directory we want the
	// folder above listed.
	if (!fs::is_directory(my_path))
	{
		my_file = my_path;
		my_path = my_path.branch_path();
		is_file = true;
	}

	// special folders like images and news append "..".
	// they want the folder above listed.
	if (my_path.leaf() == "..")
	{
		my_file = my_path.branch_path();
		for (int i=0; i<2; i++) my_path = my_path.branch_path();
		is_special_folder = true;
	}

	// handle non-directory, assume that we are in root directory
	if (!fs::is_directory(my_path))
	{
		my_path = ".";
		my_file = "." / my_file;
	}

	if (is_file && is_special_folder) throw std::logic_error("cant be file and special_folder at same time");

	if (my_path != ".")
	{ // no back button at main
		type::menuitem menu_item;
		menu_item.type = "file_folder";
		menu_item.name = "Hinauf";
		if (is_special_folder) menu_item.link = "/" + my_path.string();
		else menu_item.link = "/" + my_path.branch_path().string();
		menu_items.push_back(menu_item);
	}

	for (fs::directory_iterator dir_iter(my_path);
			dir_iter != end_iter;
			++dir_iter)
	{
		fs::path debug_path = dir_iter->path();
		hidden_t::const_iterator hit = hidden_file.find(dir_iter->leaf());
		if (hit != hidden_file.end()) continue;

		executor_t::const_iterator sit = special_file.find(dir_iter->leaf());
		if (sit != special_file.end()) continue;

		type::menuitem menu_item;
		if ((is_file || is_special_folder) && (dir_iter->path() == my_file)) menu_item.type="file_selected";
		else if (fs::is_directory(dir_iter->path())) menu_item.type = "file_folder";
		else menu_item.type = "file_html";
		menu_item.name = dir_iter->leaf();
		menu_item.link = dir_iter->leaf();
		menu_items.push_back(menu_item);
	}
}
