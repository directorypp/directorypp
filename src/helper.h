#include <string>
#include <boost/filesystem.hpp>

#include "fs.h"

std::string read_file (fs::path where, fs::path file);
fs::path find_file (fs::path where, fs::path file);
