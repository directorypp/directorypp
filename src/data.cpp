#include "data.h"

namespace content
{

message::message () :
	base(),
	title("My new homepage"),
	meta(),
	date(),

	position_items(),
	menu_items(),
	banner_items()
{
}

contentmessage::contentmessage (boost::shared_ptr<type::content> c) :
	m(c)
{}

} // end namespace data
