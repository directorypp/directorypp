#include "hello.h"
#include "helper.h"

#include "fs.h"

#include <cppcms/http_response.h>

void hello::print_default(std::string const& url)
{
	std::string file = url;

	fs::path my_path (file);

	if (fs::is_directory(my_path))
	{
		fs::directory_iterator dend;

		for (fs::directory_iterator dit(my_path); dit != dend; ++dit)
		{
			executor_t::const_iterator sit = special_file.find(dit->leaf());
			if (sit != special_file.end())
			{
				sit->second(url);
				return;
			}
		}
		// default special file executor
		executor_t::const_iterator sit = special_file.find("");
		sit->second(url);
		return;
	}

	file_information info = lookup_by_extension(file);

	if (info.binary_file)
	{
		print_file(file);
		return;
	} else {
		print_render(file);
		return;
	}
}

void hello::print_render(std::string const& url)
{
	std::string file = url;

	file_information info = lookup_by_extension(file);

	boost::shared_ptr<type::file> f;
	f = info.ptr; // the correct file type to print out the content
	f->set_file(file); // set the filename for it

	content::contentmessage c(f);
	fill_menu(file, c.menu_items);
	fill_message(file, c);
	render ("contentmaster", c);
}

void hello::print_folder(std::string const& url)
{
	std::string file = url;

	fs::path file_to_show = fs::path(url) / "index.html";
	if (!exists(file_to_show)) file_to_show = find_file(url, "standard.html");

	file_information info = lookup_by_extension(file_to_show.string()); // TODO: accept path

	boost::shared_ptr<type::file> f;
	f = info.ptr; // the correct file type to print out the content
	f->set_file(file_to_show.string()); // set the filename for it
	// TODO: above accept path

	content::contentmessage c(f);
	fill_menu(file, c.menu_items);
	fill_message(file, c);
	render ("contentmaster", c);
}

void hello::print_print(std::string const& url)
{
	std::string file = url;

	file_information info = lookup_by_extension(file);

	boost::shared_ptr<type::file> f;
	f = info.ptr; // the correct file type to print out the content
	f->set_file(file); // set the filename for it

	content::contentmessage c(f);
	fill_menu(file, c.menu_items);
	fill_message(file, c);
	render ("printmaster", c);
}

void hello::print_images(std::string const& url)
{
	std::string file = url;
	if (file.empty()) file = ".";


	content::imagemessage c;

	fill_images(file, c.image_items);

	fill_menu(file + "/..", c.menu_items);
	fill_message(file + "/..", c);
	render ("imagemaster", c);
}

void hello::print_news(std::string const& url)
{
	std::string file = url;
	if (file.empty()) file = ".";


	content::newsmessage c;

	fill_news(file, c.news_items);

	fill_menu(file + "/..", c.menu_items);
	fill_message(file + "/..", c);
	render ("newsmaster", c);
}

void hello::print_rss(std::string const& url)
{
	std::string file = url;

	response().set_content_header("text/xml");

	if (file.empty()) file = ".";


	content::newsmessage c;

	fill_news(file, c.news_items);
	// dont need to fill message here

	render ("rssmaster", c);
}

static const int BUFFER_SIZE = 4096;

void hello::print_file(std::string const& url)
{
	::std::cout << "send file " << url << ::std::endl;
	std::string file = url;
	fs::path my_path(file);

	std::string file_extension = extension(my_path);

	map::const_iterator it;
	it = transformer.find(file_extension);
	if (it == transformer.end())
	{
		throw no_transformation_for_file_extension(file_extension);
	}
	file_information info = it->second;

	response().set_content_header(info.mime_type);

	if (xsendfile == nginx)
	{
		response().set_header("X-Accel-Redirect", "/Homepage/" + file);
	} else if (xsendfile == lighttpd) {
		response().set_header("Content-Disposition", "attachment; filename=\"" + my_path.leaf() + '"');
		response().set_header("X-LIGHTTPD-send-file", root + "/" + file);
	} else if (xsendfile == apache2) {
		/* Untested, needs mod_xsendfile */
		response().set_header("Content-Disposition", "attachment; filename=\"" + my_path.leaf() + '"');
		response().set_header("X-Sendfile", root + "/" + file);
	} else {
		// fall back, send binary data of the file
		// via fcgi
		fs::ifstream fin (file);
		response().out() << fin.rdbuf();
	}

	// dont need to fill message here
	// dont need to render master here
}
