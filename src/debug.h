#ifndef DEBUG_H
#define DEBUG_H

#include <cppcms/http_request.h>

void debug_request(cppcms::http::request const& request);

#endif
