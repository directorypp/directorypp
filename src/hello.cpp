#include "hello.h"


#include "fs.h"
#include "debug.h"

#include <boost/bind.hpp>

#include <cppcms/url_dispatcher.h>
#include <cppcms/url_mapper.h>
#include <cppcms/util.h>

hello::hello (cppcms::service &srv) :
	cppcms::application(srv)
{
	root = settings().get<std::string>("server.root");
	base_url = settings().get<std::string>("server.url");
	std::string sendfile = settings().get<std::string>("server.xsendfile");
	if (sendfile == "nginx") xsendfile = nginx;
	else if (sendfile == "lighttpd") xsendfile = lighttpd;
	else if (sendfile == "apache2") xsendfile = apache2;
	else if (sendfile == "none") xsendfile = none;
	else if (!sendfile.empty())
	{
		std::cerr << "invalid value for server.xsendfile" << std::endl;
		std::cerr << "must be one of: nginx|lighttpd|apache2|none" << std::endl;
		std::cerr << "will proceed with none" << std::endl;
		xsendfile = none;
	}

	chdir (root.c_str());

	boost::shared_ptr<type::file> html_file(new type::html);
	transformer[".html"] =  file_information(html_file);

	boost::shared_ptr<type::file> text_file(new type::text);
	transformer[".desktop"] = file_information(text_file);
	transformer[".txt"] = file_information(text_file);
	transformer["."] = file_information(text_file);
	transformer[""] = file_information(text_file);

	boost::shared_ptr<type::file> code_file(new type::code);
	transformer[".xml"] = file_information(code_file);
	transformer[".txt"] = file_information(code_file);
	transformer[".php"] = file_information(code_file);

	boost::shared_ptr<type::file> cpp_file(new type::cpp);
	transformer[".cpp"] = file_information(cpp_file);
	transformer[".hpp"] = file_information(cpp_file);
	transformer[".c"] = file_information(cpp_file);
	transformer[".h"] = file_information(cpp_file);
	transformer[".C"] = file_information(cpp_file);
	transformer[".H"] = file_information(cpp_file);
	transformer[".cc"] = file_information(cpp_file);
	transformer[".hh"] = file_information(cpp_file);

	transformer[".css"] = file_information(text_file, "text/css");

	transformer[".gz"] = file_information(text_file, "application/x-gzip", true, true);

	transformer[".ico"] = file_information(text_file, "image/x-ico", true);
	transformer[".png"] = file_information(text_file, "image/png", true);
	transformer[".jpg"] = file_information(text_file, "image/jpeg", true);
	transformer[".gif"] = file_information(text_file, "image/gif", true);

	special_file[""] = boost::bind(&hello::print_folder, this, _1);
	special_file["index.html"] = boost::bind(&hello::print_folder, this, _1);
	special_file["images.html"] = boost::bind(&hello::print_images, this, _1);
	special_file["news.html"] = boost::bind(&hello::print_news, this, _1);

	hidden_file["robots.txt"] = true;
	hidden_file["standard.html"] = true;
	hidden_file["welcome.html"] = true;
	hidden_file["about.html"] = true;
	hidden_file["date.txt"] = true;
	hidden_file["favicon.ico"] = true;
	hidden_file["greeting.html"] = true;
	hidden_file["logo.png"] = true;
	hidden_file["meta.html"] = true;
	hidden_file["title.txt"] = true;
	hidden_file[".metadata"] = true;

	hidden_file["pic"] = true;
	hidden_file["banner"] = true;
	hidden_file["cppcms_rundir"] = true;

	hidden_file[".git"] = true;

	executor[""] = boost::bind(&hello::print_default, this, _1);
	executor["default"] = boost::bind(&hello::print_default, this, _1);

	executor["render"] = boost::bind(&hello::print_render, this, _1);
	executor["image"] = boost::bind(&hello::print_images, this, _1);
	executor["file"] = boost::bind(&hello::print_file, this, _1);
	executor["news"] = boost::bind(&hello::print_news, this, _1);
	executor["rss"] = boost::bind(&hello::print_rss, this, _1);
	executor["print"] = boost::bind(&hello::print_print, this, _1);


	// url.add("^/?(.*?)/?$", boost::bind(&hello::execute, this, _1));
	dispatcher().assign("^/?(.*?)/?$", &hello::execute, this, 1);
	mapper().assign("execute", "{1}");
}

void hello::execute(std::string url)
{
	debug_request(request());
	::std::cout << url << ::std::endl;

	std::string query = request().query_string();
	executor_t::const_iterator it = executor.find(query);
	if (it == executor.end()) throw no_executor_for_mode(query);

	if (url.empty()) url = ".";

	it->second(cppcms::util::urldecode(url));
}

file_information hello::lookup_by_extension(std::string const& file) const
{
	fs::path my_path(file);

	std::string file_extension = fs::extension(my_path);

	map::const_iterator it;
	it = transformer.find(file_extension);
	if (it == transformer.end())
	{
		throw no_transformation_for_file_extension(file_extension);
	}
	return it->second;
}
