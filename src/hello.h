#ifndef HELLO_H
#define HELLO_H

#include <cppcms/application.h>
#include <cppcms/applications_pool.h>
#include <cppcms/service.h>
#include <cppcms/http_response.h>

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

#include <map>
#include <fstream>
#include <iostream>

#include "data.h"

struct transformer_error : public std::exception
{};

struct no_transformation_for_file_extension : public transformer_error
{
	no_transformation_for_file_extension (std::string const& file_extension) throw() :
		m_file_extension(file_extension)
	{}

	~no_transformation_for_file_extension() throw()
	{}

	const char* what() const throw()
	{
		std::string ret = "No transformation found for file extension " + m_file_extension;
		return ret.c_str();
	}
private:
	std::string m_file_extension;
};

struct no_executor_for_mode : public transformer_error
{
	no_executor_for_mode (std::string const& mode) throw() :
		m_mode (mode)
	{}

	~no_executor_for_mode() throw()
	{}

	const char* what() const throw()
	{
		std::string ret = "No executor found for mode " + m_mode;
		return ret.c_str();
	}

private:
	std::string m_mode;
};


struct file_information
{
	file_information() :
		ptr(),
		mime_type("text/html"),
		binary_file(false),
		continue_searching(false)
	{}

	file_information(boost::shared_ptr<type::file> ptr,
			std::string mime_type = "text/html",
			bool binary_file = false,
			bool continue_searching = false) :
		ptr(ptr),
		mime_type(mime_type),
		binary_file(binary_file),
		continue_searching(continue_searching)
	{}

	boost::shared_ptr<type::file> ptr; /// the pointer to which file object should be used for transformation
	std::string mime_type; /// which mime type to send
	bool binary_file; /// there is not much text in the file
	bool continue_searching; /// found e.g .gz up to now, further investigation for complete filetype may be useful
};

class hello: public cppcms::application
{
	typedef std::map<std::string, boost::function<void(std::string const&)> > executor_t;
	executor_t executor;
	executor_t special_file; // special files are hidden files too

	typedef std::map<std::string, bool> hidden_t;
	hidden_t hidden_file;

	typedef std::map<std::string, file_information> map;
	map transformer;

	std::string root;
	std::string base_url;
	enum xsendfile_t {
		none,
		nginx,
		lighttpd,
		apache2
	} xsendfile;

	void print_debug() const;
	file_information lookup_by_extension(std::string const& file) const;

	void fill_position(std::string const& url, std::vector<type::positionitem>& position_items) const;
	void fill_message(std::string const& url, content::message & msg) const;
	void fill_images(std::string const& url, std::vector<type::imageitem>& image_items) const;
	void fill_banner(std::string const& url, std::vector<type::banneritem>& banner_items) const;
	void fill_news(std::string const& url, std::vector<type::newsitem>& news_items) const;
	void fill_menu(std::string const& url, std::vector<type::menuitem>& menu_items) const;

public:
	hello (cppcms::service &service);

	void execute(std::string url);

	void print_default(std::string const& file);
	void print_render(std::string const& file);
	void print_folder(std::string const& file);
	void print_images(std::string const& file);
	void print_news(std::string const& file);
	void print_rss(std::string const& file);
	void print_file(std::string const& file);
	void print_print(std::string const& file);
};

#endif
