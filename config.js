{
    "service" : {
        "api" : "http",
        "port" : 8080
    },
    "http" : {
        "script_names" : [ "/root" ],
        "rewrite" : [
            { "regex" : ".*", "pattern" : "/root$0" }
        ]
    },
    "server" : {
        "root" : "/home/markus/Projekte/Homepage",
        "url" : "http://localhost:8080/",
        "xsendfile" : "none",
        "generator" : "url2file",
        "author" : "Markus Raab",
        "language" : "de",
        "copyright" : "See copyright page"
    }
}
